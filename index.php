<?php
header("Content-Type: text/plain"); 

require_once("read_plugin.php");


$version = isset($_REQUEST['v']) ? $_REQUEST['v'] : '_';

$plugins = scan_plugins(dirname(__FILE__)."/plugins","",$version);

echo json_encode($plugins);
