<?php
 

/**
 * \brief Read recursively all files in $base and store them in $output
 * \param $base Name of the base directory
 * \param $file Name of a file or subdirectory of $base
 * \note  The output will contain file names relative to $base
 */
function find_plugin_files($base,$file)
{
    $output = array();
    $path = "$base/$file";
    global $plugin_api_version;
    if ( strpos($file,".") === false && is_dir($path) )
    {
        $subdirs = scandir($path);
        foreach($subdirs as $sub)
            $output = array_merge($output,find_plugin_files($base,"$file/$sub",$output));
            //$output []= $sub;
    }
    else if ( is_file($path) )
    {
        if ( $plugin_api_version == 0 )
            $output[] = file_get_contents($path);
        else 
            $output [$file]= md5(file_get_contents($path));
    }   
    return $output;
}

/**
 * \brief Read plugin data
 * \param $file Plugin JSON file
 * \return false on failure or an associative array with the data
 * \note Will add a field called plugin_last_modified with the ISO timestamp of the file
 */
function read_plugin($file)
{
    $plugin_data = json_decode(file_get_contents($file),true);
    
    if ( !isset($plugin_data['name']) )
        return false;
        
    $plugin_data['name'] = ucwords($plugin_data['name']);
    
    if ( !isset($plugin_data['type']) )
        $plugin_data['type'] = 'script';
    
    $plugin_data['type'] = strtolower($plugin_data['type']);
    
    if ( $plugin_data['type'] == 'cusp' )
        $plugin_data['category'] = 'Cusp';
    else if ( !isset($plugin_data['category']) )
        $plugin_data['category'] = 'Other';
        
    $plugin_data['plugin_last_modified'] = date("c", filectime($file));
    
 
    $plugin_data['plugin_shortname'] = basename($file,".json");
    
    if ( !isset($plugin_data['plugin_files']) || !is_array( $plugin_data['plugin_files'] ) )
    {
        $plugin_data['plugin_files'] = find_plugin_files(dirname(dirname($file)),basename(dirname($file)));
    }
        
    return $plugin_data;
}


function scan_plugins($base,$directory,$version)
{
    $path = $base;
    if ( $directory != "" )
    {
        $directory = ltrim($directory,'/');
        $path .= "/$directory";
    }
    if ( !is_dir($path) )
        return array();
        
    $plugins=array();
    $files = scandir($path);
    foreach($files as $file)
    {
        if ( strpos($file,".") === false && is_dir("$path/$file") )
        {
            $plugins = array_merge($plugins, scan_plugins($base,"$directory/$file",$version));
        }
        else
        {
            $match = array();
            if ( preg_match("/plugin_(.+)\.json/",$file,$match) )
            {
                $plugin = read_plugin("$path/$file");
                if ( $plugin["requires"] <= $version )
                {
                     $plugins[] = $plugin;//array("id"=>$match[1],"dir"=>$directory);
                }
            }
        }
    }
    
    return $plugins;
}   